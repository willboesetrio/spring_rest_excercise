package io.catalyte.training;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

  //List<Employee> getFirst(String name);

  List<Employee> findByIsActive(boolean lastName);
}
