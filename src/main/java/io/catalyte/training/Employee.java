package io.catalyte.training;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Employee {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "title")
  private String title;

  @Column(name = "is_active")
  private boolean isActive;

  @Column(name = "tenure")
  private int tenure;

  protected Employee() {
  }

  public Employee(String firstName, String lastName, String title, boolean isActive, int tenure) {
    super();
    this.firstName = firstName;
    this.lastName = lastName;
    this.title = title;
    this.isActive = isActive;
    this.tenure = tenure;
  }

  public long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(boolean isActive) {
    this.isActive = isActive;
  }

  public int getTenure() {return tenure;}

  public void setTenure(int tenure) { this.tenure = tenure;}

  @Override
  public String toString() {
    return "Employee [id="
        + id
        + ", firstName="
        + firstName
        + ", lastName="
        + lastName
        + ", title="
        + title
        + ", isActive="
        + isActive
        + ", tenure="
        + tenure
        + "]";
  }
}
