INSERT INTO employee (id, first_name, last_name, title, is_active, tenure) VALUES
  (1, 'Will', 'Boese', 'Grill Cook', false, 1),
  (2, 'first2', 'last2', 'unemployed', false, 0),
  (3, 'Frances', 'Assays', 'dev', true, 5),
  (4, 'Pat', 'Trials', 'manager', true, 3),
  (5, 'Jimmy', 'Bone', 'worker', false, 0);