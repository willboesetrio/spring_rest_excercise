CREATE TABLE employee(
	id integer auto_increment primary key,
    first_name VARCHAR(25),
    last_name VARCHAR(25),
    title varchar(25),
    is_active bool,
    tenure integer);