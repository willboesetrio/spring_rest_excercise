### Spring Rest API Exercise ###

Postman Collection:
https://api.postman.com/collections/25367406-8339fa6a-24e4-4183-a93d-7ff4134b4e12?access_key=PMAT-01GQQZBZ3K53KWVFXW6FA8K4XT

{
	"info": {
		"_postman_id": "8339fa6a-24e4-4183-a93d-7ff4134b4e12",
		"name": "spring_rest_exercise",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json",
		"_exporter_id": "25367406"
	},
	"item": [
		{
			"name": "get employees",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "http://localhost:8080/employees/",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "8080",
					"path": [
						"employees",
						""
					]
				}
			},
			"response": []
		},
		{
			"name": "add employee",
			"request": {
				"method": "POST",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\"id\":6,\"firstName\":\"test\",\"lastName\":\"vonB\",\"title\":\"teacher\",\"isActive\":true,\"tenure\":10}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://localhost:8080/employees",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "8080",
					"path": [
						"employees"
					]
				}
			},
			"response": []
		},
		{
			"name": "get employee",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "http://localhost:8080/employees/6",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "8080",
					"path": [
						"employees",
						"6"
					]
				}
			},
			"response": []
		},
		{
			"name": "delete employee",
			"request": {
				"method": "DELETE",
				"header": [],
				"url": {
					"raw": "http://localhost:8080/employees/6",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "8080",
					"path": [
						"employees",
						"6"
					]
				}
			},
			"response": []
		},
		{
			"name": "replace employee",
			"request": {
				"method": "PUT",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\"id\":6,\"firstName\":\"someone\",\"lastName\":\"else\",\"title\":\"worker\",\"isActive\":false,\"tenure\":0}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://localhost:8080/employees/6",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "8080",
					"path": [
						"employees",
						"6"
					]
				}
			},
			"response": []
		},
		{
			"name": "get active employees",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "http://localhost:8080/employees/search?isactive=true",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "8080",
					"path": [
						"employees",
						"search"
					],
					"query": [
						{
							"key": "isactive",
							"value": "true"
						}
					]
				}
			},
			"response": []
		},
		{
			"name": "get inactive employees",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "http://localhost:8080/employees/search?isactive=false",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "8080",
					"path": [
						"employees",
						"search"
					],
					"query": [
						{
							"key": "isactive",
							"value": "false"
						}
					]
				}
			},
			"response": []
		}
	]
}